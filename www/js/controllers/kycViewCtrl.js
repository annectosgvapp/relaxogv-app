/**
 * Created by atul on 3/9/2016.
 */
angular.module('kyc')
.controller('KycViewCtrl', function($state,$scope,$rootScope,$cordovaSQLite,kycServiceSaveData,$http,$q,$ionicLoading,$ionicLoading,$ionicPopup,SavephotoService,kycServiceGetData ) {
        $scope.showsMore = false;
        $scope.showButton =true;
        $scope.kyc={};

        $scope.showMoreDetails= function(rowId) {
            $state.go('tab.editkycdetail',{'rowId':rowId});
        };

        function ImageUpload(sqlite_row_id,db_kyc_id)
        {
            var q = "SELECT local_img_path,local_img_path1,local_img_path2 from  tbl_kyc where rowid =" + sqlite_row_id;
            $cordovaSQLite.execute(db, q).then(function (r) {
                if (r.rows.length > 0)
                {

                    var img_path = r.rows.item(0).local_img_path;
                    var img_path1 = r.rows.item(0).local_img_path1;
                    var img_path2 = r.rows.item(0).local_img_path2;
                    var ImageName = db_kyc_id + '_Board';
                    var ImageName1 = db_kyc_id + '_Shop';
                    var ImageName2 = db_kyc_id + '_VCard';
                    SavephotoService.Savephoto(img_path,ImageName).then(function (data) {
                        if (data.message.toString() == "image uploaded")
                        {
                            SavephotoService.Savephoto(img_path1,ImageName1).then(function (data1) {

                                if (data1.message.toString() == "image uploaded")
                                {
                                    SavephotoService.Savephoto(img_path2,ImageName2).then(function (data2) {
                                        var query = "UPDATE tbl_kyc SET uploaded = 1 where rowid =" + sqlite_row_id;

                                        $cordovaSQLite.execute(db, query);
                                        $scope.ViewLocalData();
                                    })
                                }
                            });

                        }


                    });

                }

            }, function (err) {
                console.error(JSON.stringify(err));
            })
        }

        $scope.uploadRecord= function(rowId)
        {
            if($rootScope.isOffline==true) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Message',
                    template: 'No Internet Connection! You can not Sync Records!'

                });

            }
            else{
                $scope.loading = $ionicLoading.show({
                    template: 'Uploading Please Wait...',
                    animation: 'fade-in',
                    showBackdrop: false,
                    maxWidth: 100,
                    showDelay: 0
                });

            var results = [];
            var query = "SELECT rowid, B.*, ";
            query += "(SELECT group_concat(brand_id,',') ";
            query += "FROM tbl_kyc_brand A ";
            query += "where mapping = 1 AND A.kyc_id = B.rowid GROUP BY A.kyc_id) as brnds ";
            query += "FROM tbl_kyc B where uploaded = 0 and  rowid=" + rowId;
            $cordovaSQLite.execute(db, query).then(function (res) {
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        var obj = {};
                        obj = {
                            user_id : res.rows.item(i).user_id,
                            uploaded: res.rows.item(i).uploaded,
                            device_rowid: res.rows.item(i).rowid,
                            form_no: res.rows.item(i).form_no,
                            owner_name: res.rows.item(i).owner_name,
                            bussiness_name: res.rows.item(i).bussiness_name,
                            building_name: res.rows.item(i).building_name,
                            street_name: res.rows.item(i).street_name,
                            landmark: res.rows.item(i).landmark,
                            state: res.rows.item(i).state,
                            district: res.rows.item(i).district,
                            city: res.rows.item(i).city,
                            area: res.rows.item(i).area,
                            pincode: res.rows.item(i).pincode,
                            mobile_1: res.rows.item(i).mobile_1,
                            mobile_2: res.rows.item(i).mobile_2,
                            landline: res.rows.item(i).landline,
                            email: res.rows.item(i).email,
                            website: res.rows.item(i).website,
                            shop_size: res.rows.item(i).shop_size,
                            dob: res.rows.item(i).dob,
                            image_path: res.rows.item(i).local_img_path,
                            image_path1: res.rows.item(i).local_img_path1,
                            image_path2: res.rows.item(i).local_img_path2,
                            brandlist: res.rows.item(i).brnds,
                            industry: res.rows.item(i).industry,
                            smartphone: res.rows.item(i).smartphone,
                            competitive_brands: res.rows.item(i).competition,
                            tin: res.rows.item(i).tin,
                            existing_ref_no: res.rows.item(i).existing_ref_no,
                            fe_code: res.rows.item(i).fe_code,
                            business_type: res.rows.item(i).business_type,
                            relaxo_brand_present: res.rows.item(i).relaxo_brand_present,
                            competitive_brand_present: res.rows.item(i).competitive_brand_present,
                            competition_instore_present: res.rows.item(i).competition_instore_present,
                            salesman_count: res.rows.item(i).salesman_count,
                            salesman_count: res.rows.item(i).salesman_count,
                            intSellRelaxoProd : res.rows.item(i).intSellRelaxoProd
                        }

                        if (obj.uploaded == 0) {
                            results.push(obj);

                        }

                    }

                    $scope.kyc_rec = results;


                    kycServiceSaveData.SaveDataMySQL($http, $q, $scope.kyc_rec).then(function (data) {


                            if (data.length > 0) //Sucessfully inserted Data
                            {

                                for (var i = 0; i < data.length; i++) {

                                    if (data[i].kyc_id != 0) {
                                        var db_kyc_id = data[i].kyc_id;
                                        var sqlite_row_id = data[i].id;
                                        var query = "UPDATE tbl_kyc SET db_key=" + data[i].kyc_id + " where rowid =" + data[i].id;

                                        $cordovaSQLite.execute(db, query);
                                        ImageUpload(sqlite_row_id, db_kyc_id);

                                    }

                                }


                                $ionicLoading.hide();

                                var alertPopup = $ionicPopup.alert({
                                    title: 'Message',
                                    template: 'Your Records are Saved in Remote Server '
                                });

                            }


                        },
                        function (err) {
                            console.error(JSON.stringify(err));
                        });


                }


            }, function (err) {
                console.error(JSON.stringify(err));
            });
        }
     }
        $scope.ViewLocalData= function () {

            var brndlist = "";
            var rslt = [];
            var brand_rslt = [];
            var total_upload = 0;

            var query = "SELECT rowid, B.*, ";
            query += "(SELECT group_concat(brand_name,',') ";
            query += "FROM tbl_kyc_brand A inner join mstr_brands C on C.brndid = A.brand_id ";
            query += "where mapping = 1 AND A.kyc_id = B.rowid GROUP BY A.kyc_id) as brnds ";
            query += "FROM tbl_kyc B where user_id="+ $rootScope.user_id ;
            query +=  " order by B.id desc";
             var res = $cordovaSQLite.execute(db, query).then(function (res) {
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {

                        var obj = {};
                        obj ={"upload_status": false }
                        obj = {
                            uploaded: res.rows.item(i).uploaded,
                            row_id: res.rows.item(i).rowid,
                            owner_name: res.rows.item(i).owner_name,
                            bussiness_name: res.rows.item(i).bussiness_name,
                            building_name: res.rows.item(i).building_name,
                            state: res.rows.item(i).state,
                            city: res.rows.item(i).city,
                            mobile_1: res.rows.item(i).mobile_1,
                            approval_status: res.rows.item(i).approval_status,
                            fe_code: res.rows.item(i).fe_code
                        }
                        if(obj.approval_status == 0)
                        {
                            obj.approval_status ='Pending';
                        }
                        if(obj.approval_status == 1)
                        {
                            obj.approval_status ='Approved';
                        }
                        if(obj.approval_status == 2)
                        {
                            obj.approval_status ='Rejected';
                        }

                        if (obj.uploaded == 0) {
                            obj.uploaded = 'No';
                        }
                        else {
                            obj.uploaded = 'Yes';
                            obj.upload_status = true;
                        }
                        rslt.push(obj);
                    }
                    $scope.kyc_lists = rslt;

                    $scope.$root.kyc_lists_count = rslt.length;
                    $scope.total_upload = total_upload + rslt.length;
                }
                $scope.qty = rslt.length;
                $scope.$watch(function (scope) {
                        return scope.qty
                    },
                    function (newValue, oldValue) {
                        $scope.$root.kyc_lists_count = newValue;
                    }
                );

            },
            function (err) {
                console.error(JSON.stringify(err));
            });


        }


        init();
    function init(){

        $scope.ViewLocalData();
    }

})