/**
 * Created by atul on 3/9/2016.
 */
angular.module('kyc')
    .config(function ($compileProvider) {
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
    })

    .controller('KycAddCtrl', function ($rootScope, $scope, $cordovaSQLite, $ionicPopup, $location, $rootScope, $state, $q, $cordovaCamera, $cordovaFile) {

        $scope.kyc = {};

        $scope.SaveFormData = function (kyc, Kyc_form) {
            var compBrand_str;

            if (Kyc_form.$valid) {
                $scope.kyc = angular.copy(kyc);
                if ($scope.BoardImage !== undefined && $scope.ShopInside !== undefined && $scope.OwnerId !== undefined) {
                    if($scope.kyc.competition!== undefined){
                        compBrand_str =  $scope.objToString($scope.kyc.competition);
                    }
                    else
                    {
                        compBrand_str='';
                    }
                    var query = "INSERT INTO tbl_kyc (form_no,owner_name, bussiness_name,building_name,street_name,landmark,state,district,city,area,pincode,mobile_1,mobile_2,landline,email,website,shop_size,dob,local_img_path,local_img_path1,local_img_path2,user_id,smartphone,competition,tin,existing_ref_no,business_type,relaxo_brand_present,competitive_brand_present,competition_instore_present,salesman_count,intSellRelaxoProd) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    $cordovaSQLite.execute(db, query, [$scope.kyc.form_no, $scope.kyc.owner_name, $scope.kyc.bussiness_name, $scope.kyc.building_name, $scope.kyc.street_name, $scope.kyc.landmark, $scope.kyc.area.state, $scope.kyc.area.district, $scope.kyc.area.city, $scope.kyc.area2, $scope.kyc.pincode, $scope.kyc.Mobile1, $scope.kyc.Mobile2, $scope.kyc.landline, $scope.kyc.Email, $scope.kyc.Website, $scope.kyc.shop_size, $scope.kyc.dob, $scope.BoardImage, $scope.ShopInside, $scope.OwnerId, $rootScope.user_id, $scope.kyc.smartphone,compBrand_str,$scope.kyc.tin, $scope.kyc.existing_ref_no, $scope.kyc.business_type, $scope.kyc.relaxo_brand_present, $scope.kyc.competitive_brand_present, $scope.kyc.competition_instore_present, $scope.kyc.salesman_count,$scope.kyc.intSellRelaxoProd]).then(function (res) {
                        console.log("INSERT ID -> " + res.insertId);
                        var insertedID = res.insertId;
                        var user_id  = $rootScope.user_id;
                        function codeGeneration(user_id,insertedID){

                            var randomChar= "";

                            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

                            if ( user_id <=9999 && insertedID <=9999) {
                                user_id = ("000"+user_id).slice(-4);
                                insertedID =("000"+insertedID).slice(-4)
                            }


                            for( var i=0; i < 2; i++ ) {

                                randomChar += possible.charAt(Math.floor(Math.random() * possible.length));

                            }

                            return   user_id + '-' + insertedID +  '-' + randomChar;

                        }
                        $scope.code= codeGeneration(user_id,insertedID);
                        var query="UPDATE tbl_kyc SET fe_code = " +"'"+ $scope.code + "'"+ " where rowid =" + res.insertId ;

                        $cordovaSQLite.execute(db, query).then(function(res) {
                                console.log('Code generated ')
                            },
                            function (err) {
                                console.error(JSON.stringify(err));
                            });

                        $scope.kyc.BoardImageName = $scope.kyc.form_no + '-BoardImage';
                        $scope.Kyc_form = angular.copy(kyc);
                        var qry = "INSERT INTO tbl_kyc_brand ( kyc_id , brand_id , mapping ) VALUES (?,?,?)";
                        $cordovaSQLite.execute(db, qry, [res.insertId, 1 , $scope.kyc.s1 == 1 ? 1 : 0]);
                        $cordovaSQLite.execute(db, qry, [res.insertId, 2 , $scope.kyc.s2 == 1 ? 1 : 0]);
                        $cordovaSQLite.execute(db, qry, [res.insertId, 3 , $scope.kyc.s3 == 1 ? 1 : 0]);
                        $cordovaSQLite.execute(db, qry, [res.insertId, 4 , $scope.kyc.s4 == 1 ? 1 : 0]);
                        $cordovaSQLite.execute(db, qry, [res.insertId, 5 , $scope.kyc.s5 == 1 ? 1 : 0]);
                        $cordovaSQLite.execute(db, qry, [res.insertId, 6 , $scope.kyc.s6 == 1 ? 1 : 0]);
                        $cordovaSQLite.execute(db, qry, [res.insertId, 7 , $scope.kyc.s7 == 1 ? 1 : 0]);
                        $cordovaSQLite.execute(db, qry, [res.insertId, 8 , $scope.kyc.s8 == 1 ? 1 : 0]);
                        $cordovaSQLite.execute(db, qry, [res.insertId, 9 , $scope.kyc.s9 == 1 ? 1 : 0]);
                        $cordovaSQLite.execute(db, qry, [res.insertId, 10 , $scope.kyc.s10 == 1 ? 1 : 0]);
                        $cordovaSQLite.execute(db, qry, [res.insertId, 11 , $scope.kyc.s11 == 1 ? 1 : 0]);
                        $cordovaSQLite.execute(db, qry, [res.insertId, 12 , $scope.kyc.s12 == 1 ? 1 : 0]);
                        $cordovaSQLite.execute(db, qry, [res.insertId, 13 , $scope.kyc.s13 == 1 ? 1 : 0]);

                        var alertPopup = $ionicPopup.alert({
                            title: 'Message',
                            template: 'Your Data has been saved in your local Storage. '+'Your Code is  ' + $scope.code

                        });

                        alertPopup.then(function (res) {
                            $scope.Kyc_form = angular.copy(kyc);
                            $state.go('tab.kycview');
                        });

                    }, function (err) {
                        console.error(JSON.stringify(err));
                    });
                }

                else {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Message',
                        template: 'Photo is Required !'

                    });

                }
            }


        }

        $scope.takePhotoShop = function () {
            var options = {

                destinationType: Camera.DestinationType.NATIVE_URI,

            };
            $cordovaCamera.getPicture(options).then(function (sourcePath) {
                var sourceDirectory = sourcePath.substring(0, sourcePath.lastIndexOf('/') + 1);
                var sourceFileName = sourcePath.substring(sourcePath.lastIndexOf('/') + 1, sourcePath.length);
                $cordovaFile.copyFile(sourceDirectory, sourceFileName, cordova.file.dataDirectory, sourceFileName).then(function (success) {
                    $scope.BoardImage = cordova.file.dataDirectory + sourceFileName;
                }, function (error) {
                    console.dir(error);
                });

            });


        }


        $scope.takePhotoShopInside = function () {
            var options = {

                destinationType: Camera.DestinationType.NATIVE_URI,

            };
            $cordovaCamera.getPicture(options).then(function (sourcePath) {

                var sourceDirectory = sourcePath.substring(0, sourcePath.lastIndexOf('/') + 1);
                var sourceFileName = sourcePath.substring(sourcePath.lastIndexOf('/') + 1, sourcePath.length);
                $cordovaFile.copyFile(sourceDirectory, sourceFileName, cordova.file.dataDirectory, sourceFileName).then(function (success) {
                    $scope.ShopInside = cordova.file.dataDirectory + sourceFileName;

                }, function (error) {
                    console.dir(error);
                });

            });


        }

        $scope.takePhotoOwnerId = function () {
            var options = {

                destinationType: Camera.DestinationType.NATIVE_URI,

            };
            $cordovaCamera.getPicture(options).then(function (sourcePath) {

                var sourceDirectory = sourcePath.substring(0, sourcePath.lastIndexOf('/') + 1);
                var sourceFileName = sourcePath.substring(sourcePath.lastIndexOf('/') + 1, sourcePath.length);
                $cordovaFile.copyFile(sourceDirectory, sourceFileName, cordova.file.dataDirectory, sourceFileName).then(function (success) {
                    $scope.OwnerId = cordova.file.dataDirectory + sourceFileName;

                }, function (error) {
                    console.dir(error);
                });

            });


        }


        $scope.bindUserAllocation = function () {

            var results = [];
            var i = 0;
            var obj = {};
            var qry = "SELECT DISTINCT pincode from  user_allocation";

            $cordovaSQLite.execute(db, qry).then(function (res) {

                if (res.rows.length > 0) {
                    for (i = 0; i < res.rows.length; i++) {
                        obj = {
                            pincode: res.rows.item(i).pincode
                        }
                        results.push(obj);
                    }
                    $scope.UserAllocation_lists = results;

                }
            })
        }

        $scope.BindArea = function (pincd) {
            var results = [];

            var qry = "SELECT *  from  user_allocation where pincode = '" + pincd + "'";
            $cordovaSQLite.execute(db, qry).then(function (res) {
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {

                        var obj = {};
                        obj = {

                            state: res.rows.item(i).state,
                            district: res.rows.item(i).district,
                            city: res.rows.item(i).city,
                            area: res.rows.item(i).area,


                        }
                        results.push(obj);
                    }
                    $scope.Arealist = results;

                }
            })
        }

        $scope.populatetext = function () {
            $scope.kyc.area2 = $scope.kyc.area.area;
        }

        $scope.objToString = function(obj){
            var str = '';
            for (var p in obj) {
                if (obj.hasOwnProperty(p)) {
                    str += p + ',' ;
                }
            }
            return str;
        }

        init();
        function init() {
            $scope.bindUserAllocation();
        }
    })
