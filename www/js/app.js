
angular.module('kyc', ['ionic','kyc.services','ngCordova','ngMessages'])

.run(function($ionicPlatform,$cordovaSQLite,$cordovaNetwork,$interval,$ionicPopup,$rootScope) {
  $ionicPlatform.ready(function() {

      // Check for network connection
      document.addEventListener("deviceready", function () {


          var type = $cordovaNetwork.getNetwork()

          $rootScope.isOnline = $cordovaNetwork.isOnline()

          $rootScope.isOffline = $cordovaNetwork.isOffline()

          if($rootScope.isOffline==true)
          {
              var alertPopup = $ionicPopup.alert({
                  title: 'Message',
                  template: 'No Internet Connection! Please Turn ON your Internet for Login'
              });

          }
          // listen for Online event
          $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
              var onlineState = networkState;
              $rootScope.isOnline= true;
              $rootScope.isOffline= false;

          })

          // listen for Offline event
          $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
              var offlineState = networkState;
              $rootScope.isOnline= false;
              $rootScope.isOffline= true;
              var alertPopup = $ionicPopup.alert({
                  title: 'Message',
                  template: 'No Internet Connection! But Still You Can Fill KYC Add Form and Save Locally'
              });
          })

      }, false);


    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {

      StatusBar.styleDefault();
    }

    db = $cordovaSQLite.openDB("relaxo.kyc");

      console.log("DB Created");
      $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS tbl_kyc ( `id` INT PRIMARY KEY,`form_no` VARCHAR(25) NULL DEFAULT NULL, `owner_name` VARCHAR(100) NULL DEFAULT NULL,`bussiness_name` VARCHAR(100) NULL DEFAULT NULL,`building_name` VARCHAR(100) NULL DEFAULT NULL,`street_name` VARCHAR(100) NULL DEFAULT NULL,`landmark` VARCHAR(100) NULL DEFAULT NULL,`state` VARCHAR(45) NULL DEFAULT NULL,`district` VARCHAR(45) NULL DEFAULT NULL,`city` VARCHAR(45) NULL DEFAULT NULL,`area` VARCHAR(45) NULL DEFAULT NULL,`pincode` BIGINT(10) NULL DEFAULT NULL,`mobile_1` BIGINT(15) NULL DEFAULT NULL,`mobile_2` BIGINT(15) NULL DEFAULT NULL,`landline` BIGINT(15) NULL DEFAULT NULL,`email` VARCHAR(45) NULL DEFAULT NULL,`website` VARCHAR(150) NULL DEFAULT NULL,`shop_size` VARCHAR(45) NULL DEFAULT NULL,`dob` VARCHAR(45) NULL DEFAULT NULL,`user_id` VARCHAR(45) NULL DEFAULT NULL,`ignore_flag` BIT(1) NULL DEFAULT NULL,`ignore_user_id` VARCHAR(45) NULL DEFAULT NULL,`ignore_date` DATETIME NULL DEFAULT NULL,`lat` DECIMAL(12,8) NULL DEFAULT NULL,`long` DECIMAL(12,8) NULL DEFAULT NULL,`device_id` VARCHAR(45) NULL DEFAULT NULL,`db_key` INT(11) NULL DEFAULT NULL,`brand_list` VARCHAR(250) NULL DEFAULT NULL,`approval_status` INT  DEFAULT 0,`reject_reason` VARCHAR(25) NULL DEFAULT NULL,`local_img_path` VARCHAR(250) NULL DEFAULT NULL,`local_img_path1` VARCHAR(250) NULL DEFAULT NULL,`local_img_path2` VARCHAR(250) NULL DEFAULT NULL,`uploaded` BOOLEAN  DEFAULT 0,`industry` VARCHAR(100) NULL DEFAULT NULL,`reject_remarks` VARCHAR(250) NULL DEFAULT NULL,`smartphone` VARCHAR(50) NULL DEFAULT NULL,`competition` VARCHAR(250) NULL DEFAULT NULL,`tin` VARCHAR(100) NULL DEFAULT NULL,`existing_ref_no` VARCHAR(50) NULL DEFAULT NULL,`fe_code` VARCHAR(150) NULL DEFAULT NULL,`business_type` VARCHAR(50) NULL DEFAULT NULL,`relaxo_brand_present`  BIT(1) NULL DEFAULT NULL,`competitive_brand_present`  BIT(1) NULL DEFAULT NULL,`competition_instore_present` BIT(1) NULL DEFAULT NULL,`salesman_count` INT NULL DEFAULT NULL,`intSellRelaxoProd` VARCHAR(50) NULL DEFAULT NULL   ) " );
      $cordovaSQLite.execute(db,"CREATE TABLE IF NOT EXISTS `tbl_kyc_brand` ( `kyc_id` INT(11) NULL DEFAULT NULL,`brand_id` INT(11) NULL DEFAULT NULL,`mapping` BOOLEAN NOT NULL DEFAULT 0 )");
      $cordovaSQLite.execute(db,"CREATE TABLE IF NOT EXISTS `mstr_brands` (`brndid` INT(11) NOT NULL,`brand_name` VARCHAR(100) NULL)");
      $cordovaSQLite.execute(db,"CREATE TABLE IF NOT EXISTS `user_allocation` (`userAreaID` INT(11) NOT NULL,`areaid` INT(11) NOT NULL,`state` VARCHAR(100) NULL,`district` VARCHAR(100) NULL,`city` VARCHAR(100) NULL,`area` VARCHAR(100) NULL,`pincode` VARCHAR(100) NULL)");
      $cordovaSQLite.execute(db,"CREATE TABLE IF NOT EXISTS `tbl_users` (`userid` VARCHAR(50) NULL DEFAULT NULL, `username` VARCHAR(100) NULL DEFAULT NULL,`password` VARCHAR(100) NULL DEFAULT NULL)");
      console.log("Tables Created");
      $cordovaSQLite.execute(db,"DELETE from mstr_brands" );
      $cordovaSQLite.execute(db,"insert into mstr_brands values (1, 'Hawaii')");
      $cordovaSQLite.execute(db,"insert into mstr_brands values (2, 'Belle')");
      $cordovaSQLite.execute(db,"insert into mstr_brands values (3, 'Bahamas')");
      $cordovaSQLite.execute(db,"insert into mstr_brands values (4, 'Flite')");
      $cordovaSQLite.execute(db,"insert into mstr_brands values (5, 'Flite PU')");
      $cordovaSQLite.execute(db,"insert into mstr_brands values (6, 'Sparx Sandals')");
      $cordovaSQLite.execute(db,"insert into mstr_brands values (7, 'Sparx Fabricated')");
      $cordovaSQLite.execute(db,"insert into mstr_brands values (8, 'Sparx Shoes')");
      $cordovaSQLite.execute(db,"insert into mstr_brands values (9, 'Sparx Canvas')");
      $cordovaSQLite.execute(db,"insert into mstr_brands values (10, 'Schoolmate')");
      $cordovaSQLite.execute(db,"insert into mstr_brands values (11, 'Casual')");
      $cordovaSQLite.execute(db,"insert into mstr_brands values (12, 'Sparx DIP')");
      $cordovaSQLite.execute(db,"insert into mstr_brands values (13, 'Canvas')");

  });
})

    .config(function($stateProvider, $urlRouterProvider) {

      $stateProvider

        .state('login', {
              url: '/login',
              templateUrl: 'templates/login.html',
              controller: 'LoginCtrl'
          })

        .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
      })

      .state('tab.dash', {
        cache: false,
        url: '/dash',
        views: {
          'tab-dash': {
            templateUrl: 'templates/tab-dash.html',
            controller: 'DashBoardCtrl'
          }
        }
      })

      .state('tab.kycadd', {
          cache: false,
          url: '/kycadd',
          views: {
            'tab-kycadd': {
              templateUrl: 'templates/tab-kycadd.html',
               controller: 'KycAddCtrl'
            }
          }
        })
      .state('tab.kycview', {
             cache: false,
              parent: "tab",
              url: '/kycview',
              views: {
                  'tab-kycview': {
                      templateUrl: 'templates/tab-kycview.html',
                      controller: 'KycViewCtrl'
                  }
              }
          })

        .state('tab.editkycdetail', {
          url: '/kycedit/:rowId',
          views: {
            'tab-kycview': {
              templateUrl: 'templates/edit-kyc.html',
                controller: 'EditCtrl'

            }
          }
        })

        $urlRouterProvider.otherwise('/login');

})
