angular.module('kyc.services', [])

    .factory('Camera', ['$q', function($q) {

        return {
            getPicture: function(options) {
                var q = $q.defer();

                navigator.camera.getPicture(function(result) {
                    // Do any magic you need
                    q.resolve(result);
                }, function(err) {
                    q.reject(err);
                }, options);

                return q.promise;
            }
        }
    }])

    .service('kycServiceSaveData', function () {
        this.SaveDataMySQL = function ($http, $q,dataobj) {
          var apiPath = 'http://app.annectos.net/ecomm.gv_relaxo.api/category/kyc/kyc_add';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: dataobj,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occurred Saving Data");
            })
            return deferred.promise;
        };
    })

    .service('kycServiceGetData', function () {
        this.SelectKycByUserId = function ($http, $q,user_id) {
            var apiPath = 'http://app.annectos.net/ecomm.gv_relaxo.api/category/admin/SelectKycByUserId?user_id=' + user_id;
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occurred Saving Data");
            })
            return deferred.promise;
        };

        this.SelectUserAllocation = function ($http, $q,user_id) {
            var apiPath = 'http://app.annectos.net/ecomm.gv_relaxo.api/category/admin/SelectUserAllocation/?user_id=' + user_id +'&areaid=4';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                type: JSON
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("An error occurred Saving Data");
            })
            return deferred.promise;
        };



    })

    .service('SavephotoService', function () {
        this.Savephoto = function (imageURI,BoardImage)  {
            var s3URI = encodeURI("https://s3-ap-southeast-1.amazonaws.com/gv-relaxo/"),
                policyBase64 = "eyJleHBpcmF0aW9uIjoiMjAyMC0xMi0zMVQxMjowMDowMC4wMDBaIiwiY29uZGl0aW9ucyI6W3siYnVja2V0IjoiZ3YtcmVsYXhvIn0sWyJzdGFydHMtd2l0aCIsIiRrZXkiLCIiXSx7ImFjbCI6InB1YmxpYy1yZWFkIn0sWyJzdGFydHMtd2l0aCIsIiRDb250ZW50LVR5cGUiLCIiXSxbImNvbnRlbnQtbGVuZ3RoLXJhbmdlIiwwLDUyNDI4ODAwMF1dfQ==",
                signature = "Unt5Mw49qkDNZp9XjYbwJHpCPas=",
                awsKey='AKIAJEKVUP2XVSXTZ4NQ',
                acl = "public-read";
                var deferred  = $.Deferred(),
                    ft = new FileTransfer(),
                    options = new FileUploadOptions();

                options.fileKey = "file";
                options.BoardImage = BoardImage;
                options.mimeType = "image/jpeg";

                options.chunkedMode = false;
                options.params = {
                    "key": BoardImage,
                    "AWSAccessKeyId": awsKey,
                    "acl": acl,
                    "policy": policyBase64,
                    "signature": signature,
                    "Content-Type": "image/jpeg"
                };


                ft.upload(imageURI, s3URI,
                    function (e) {
                        e.message='image uploaded';
                        deferred.resolve(e);
                       console.log( 'image uploaded');

                    },
                    function (e) {
                        e.message='image NOT uploaded';
                        deferred.reject(e);
                        console.log('image NOT uploaded');

                    }, options);

                return deferred.promise();


        }

    })

    .service('AuthService', function() {

        this.login = function($http, $q,name, pw) {
                    if (name !== undefined ) {
                        name = name.toLowerCase();
                    }
                    var dataobj = {};
                        dataobj={
                            "user_name": name,
                            "password" : pw
                        }
                    if ( dataobj !== undefined) {

                        var apiPath = 'http://app.annectos.net/ecomm.gv_relaxo.api/category/admin/kyc_login/';
                        var deferred = $q.defer();
                        $http({
                            method: 'POST',
                            url: apiPath,
                            data: dataobj,
                            type: JSON
                        }).success(function (data) {

                            deferred.resolve(data);

                        }).error(function (data) {
                            console.log('err ' + data)
                            deferred.reject("An error occurred ");
                        })
                        return deferred.promise;

                    resolve('Login success.');
                } else {
                    reject('Login Failed.');
                }

        };

    })

